import pytest
import numpy as np

def test_numpy():
    a = np.array([1, 2, 3])
    assert a.size == 3

def test_numpy2():
    b = np.array([1,2,3])
    c = np.array([1, 2, 3])
    assert np.array_equal(b,c)